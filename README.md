# We have migrated our projects off of GitLab and onto GitHub.

If you have bug reports for the core software, they can be filed in the appropriate bug reporting channels and our logged issues and future plans can be seen here: https://github.com/foundryvtt/foundryvtt/issues

If you have issues with one of our first-party produced premium content packages, such as The Demon Queen Awakens, or The Pathfinder 2e Beginner Box or Abomination Vaults, you may file issues for those here: https://github.com/foundryvtt/foundryvtt-premium-content/issues